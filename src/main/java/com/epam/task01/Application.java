package com.epam.task01;

import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        CustomInterface customInterface = (a, b, c) -> Stream.of(a, b, c).max(Integer::compareTo).get();
        System.out.println(customInterface.foo(1, 3, 2));

        customInterface = (a, b, c) -> (int) Stream.of(a, b, c)
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
        System.out.println(customInterface.foo(5, 2, 5));
    }
}
