package com.epam.task01;

@FunctionalInterface
public interface CustomInterface {
    int foo(int a, int b, int c);
}
