package com.epam.task04;

import com.epam.task04.utils.StreamUtils;

import java.util.*;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text;
        List<String> words;
        while(!(text = scanner.nextLine()).isEmpty()) {
            words = Arrays.asList(text.split(" "));

            List<String> uniqueWords = StreamUtils.getUniqueWords(words);
            long uniqueWordsCount = uniqueWords.size();
            System.out.println("Unique words count = " + uniqueWordsCount);

            List<String> sortedUniqueWords = StreamUtils.sortedUniqueWords(words);
            System.out.println("Sorted list of unique words: ");
            System.out.println(sortedUniqueWords);

            Map<String, Long> wordCount = StreamUtils.wordCount(words);
            System.out.println("Word count:");
            System.out.println(wordCount);

            Map<String, Long> charCount = StreamUtils.charactersCount(text);

            System.out.println("Characters count: ");
            System.out.println(charCount);
        }
    }
}
