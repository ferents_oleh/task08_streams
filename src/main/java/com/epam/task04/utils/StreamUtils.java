package com.epam.task04.utils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamUtils {
    public static List<String> getUniqueWords(List<String> words) {
        return words.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() == 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static List<String> sortedUniqueWords(List<String> uniqueWords) {
        return uniqueWords
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public static Map<String, Long> wordCount(List<String> words) {
        return words.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public static Map<String, Long> charactersCount(String word) {
        return word.codePoints()
                .mapToObj(Character::toString)
                .filter(ch -> !ch.equals(" "))
                .filter((ch -> !ch.equals(ch.toUpperCase())))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
