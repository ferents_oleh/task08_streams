package com.epam.task02.view;

import com.epam.task02.command.Command;
import com.epam.task02.command.impl.OpenFileCommand;
import com.epam.task02.command.impl.SaveFileCommand;
import com.epam.task02.command.impl.TurnOffLightCommand;
import com.epam.task02.command.impl.TurnOnLightCommand;
import com.epam.task02.invoker.CommandInvoker;
import com.epam.task02.receiver.Light;
import com.epam.task02.receiver.TextFile;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private CommandInvoker commandInvoker;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu(CommandInvoker commandInvoker) {
        this.commandInvoker = commandInvoker;

        Menu menu = new Menu("Task 02", "menu");

        menu.putAction("Turn on light", this::turnOnLight);
        menu.putAction("Turn off light", this::turnOffLight);
        menu.putAction("Open file", this::openFile);
        menu.putAction("Save file", this::saveFile);

        activateMenu(menu);
    }

    private void turnOnLight() {
        commandInvoker.executeCommand(new TurnOnLightCommand(new Light()));
    }

    private void turnOffLight() {
        commandInvoker.executeCommand(()-> new TurnOffLightCommand(new Light()).execute());
    }

    private void openFile() {
        commandInvoker.executeCommand(new Command() {
            @Override
            public void execute() {
                new OpenFileCommand(new TextFile("info.txt")).execute();
            }
        });
    }

    private void saveFile() {
        SaveFileCommand saveFileCommand = new SaveFileCommand(new TextFile("test.txt"));
        commandInvoker.executeCommand(saveFileCommand::execute);
    }



    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }
    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }

}
