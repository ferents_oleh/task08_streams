package com.epam.task02.receiver;

public class TextFile {
    private String name;

    public TextFile(String name) {
        this.name = name;
    }

    public void open() {
        System.out.println("Opening " + name);
    }

    public void save() {
        System.out.println("Saving " + name);
    }
}
