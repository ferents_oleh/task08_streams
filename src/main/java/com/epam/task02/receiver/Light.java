package com.epam.task02.receiver;

public class Light {
    public void turnOn() {
        System.out.println("Turning on...");
    }

    public void turnOff() {
        System.out.println("Turning off...");
    }
}
