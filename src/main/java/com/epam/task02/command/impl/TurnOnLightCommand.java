package com.epam.task02.command.impl;

import com.epam.task02.command.Command;
import com.epam.task02.receiver.Light;

public class TurnOnLightCommand implements Command {
    private Light light;

    public TurnOnLightCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.turnOn();
    }
}
