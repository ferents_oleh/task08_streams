package com.epam.task02.command.impl;

import com.epam.task02.command.Command;
import com.epam.task02.receiver.TextFile;

public class OpenFileCommand implements Command {
    private TextFile textFile;

    public OpenFileCommand(TextFile textFile) {
        this.textFile = textFile;
    }

    @Override
    public void execute() {
        textFile.open();
    }
}
