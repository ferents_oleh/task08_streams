package com.epam.task02.command.impl;

import com.epam.task02.command.Command;
import com.epam.task02.receiver.TextFile;

public class SaveFileCommand implements Command {
    private TextFile textFile;

    public SaveFileCommand(TextFile textFile) {
        this.textFile = textFile;
    }

    @Override
    public void execute() {
        textFile.save();
    }
}
