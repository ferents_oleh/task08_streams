package com.epam.task02.command;

@FunctionalInterface
public interface Command {
    void execute();
}
