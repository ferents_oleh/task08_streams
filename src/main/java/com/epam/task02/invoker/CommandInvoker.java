package com.epam.task02.invoker;

import com.epam.task02.command.Command;

public class CommandInvoker {

    public void executeCommand(Command command) {
        command.execute();
    }




//    private Command turnOnLight;
//
//    private Command turnOffLight;
//
//    private Command openFile;
//
//    private Command saveFile;
//
//    public CommandInvoker(Command turnOnLight, Command turnOffLight, Command openFile, Command saveFile) {
//        this.turnOnLight = turnOnLight;
//        this.turnOffLight = turnOffLight;
//        this.openFile = openFile;
//        this.saveFile = saveFile;
//    }
//
//    public void turnOnLight() {
//        turnOnLight.execute();
//    }
//
//    public void turnOffLight() {
//        turnOffLight.execute();
//    }
//
//    public void openFile() {
//        openFile.execute();
//    }
//
//    public void saveFile() {
//        saveFile.execute();
//    }
}
