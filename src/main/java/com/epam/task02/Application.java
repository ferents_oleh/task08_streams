package com.epam.task02;

import com.epam.task02.invoker.CommandInvoker;
import com.epam.task02.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(new CommandInvoker());
    }
}
