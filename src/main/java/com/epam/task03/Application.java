package com.epam.task03;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        List<Integer> list = IntegerList.generateList(10, 0, 9);
        System.out.println("List:");
        System.out.println(list);
        System.out.println("Max value:" + IntegerList.max(list));
        System.out.println("Min value:" + IntegerList.min(list));
        System.out.println("Average value:" + IntegerList.average(list));
        System.out.println("Sum:" + IntegerList.sum(list));
        System.out.println("Count of values bigger than average value:"
                + IntegerList.countValuesBiggerThanAverage(list, IntegerList.average(list)));
    }
}
