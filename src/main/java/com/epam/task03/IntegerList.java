package com.epam.task03;

import java.util.*;
import java.util.stream.Collectors;

public class IntegerList {
    public static List<Integer> generateList(int limit, int low, int high) {
        return new Random()
                .ints(limit, low, high)
                .boxed()
                .collect(Collectors.toList());
    }

    private static IntSummaryStatistics getStat(List<Integer> list) {
        return list.stream().mapToInt(Integer::intValue).summaryStatistics();
    }

    public static double average(List<Integer> list) {
        return getStat(list)
                .getAverage();
    }

    public static int max(List<Integer> list) {
        return getStat(list)
                .getMax();
    }

    public static int min(List<Integer> list) {
        return getStat(list)
                .getMin();
    }

    public static long sum(List<Integer> list) {
        return getStat(list)
                .getSum();
    }

    public static long countValuesBiggerThanAverage(List<Integer> list, double average) {
        return list
                .stream()
                .filter(i -> i > average).count();
    }
}
